console.log("S17 ACTIVITY");

/*1. TASK 1: Create an addStudent() function that will accept a name of the student and
			 add it to the student array.*/

let sections = [];

function addStudent(studentName) {
    sections.push(studentName);
  
}

/*TASK 2: Create a countStudents() function that 
will count the number of names in the student array.*/

function printStudentCount(){
	console.log(sections.length);
}

/*TASK 3: Create a printStudents() function that 
will sort and print individually the names*/

//Displaying names added with the use of sort

function printSections(){
	sections.sort();
	sections.forEach(function (student){
			console.log(student);
		})
}

/* Task 4: Create a findStudent() function that 
will search for a student name when a keyword is given.*/
//One match print '(name of student)' is an enrollee.
//Two or more '(name of students)' are enrollees.
//No match '(name of student)' is not enrollee.
//keyword should not be case sensitive 

function findStudent(sectionfilter) {

    let searchName =  sections.filter( function(student) {
        
        return (student.toLowerCase())===(sectionfilter.toLowerCase());

     });

     if (searchName.length == 1){
       console.log(`${sectionfilter} is an enrollee.`);
     }
     else if(searchName.length > 1){
        console.log(`${sectionfilter}  are  enrollees`);
     }
     else{
        console.log(`${sectionfilter} is not an enrollee.`)
     }
}